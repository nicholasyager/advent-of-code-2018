use std::env;
use std::fs::File;
use std::io::BufRead;
use std::io::BufReader;

use std::collections::{HashMap, HashSet};

fn main() {
    let args: Vec<String> = env::args().collect();
    let filename: &String = args.get(1).expect("You must supply a file to evaluate.");

    let f = File::open(filename).expect("File not found");
    let file = BufReader::new(&f);
    let lines: Vec<String> = file.lines().map(|l| l.unwrap()).collect();
    println!("Processing file {:?}", filename);

    let mut letter_counts: [u32; 50] = [0; 50];

    for line in &lines {
        let characters: Vec<char> = line.chars().collect();
        let mut character_counts: HashMap<char, u32> = HashMap::new();
        let mut counts_used: HashSet<u32> = HashSet::new();
        for character in characters {
            let count = character_counts.entry(character).or_insert(0);
            *count += 1;
        }
        for (_, val) in character_counts.iter() {
            if !counts_used.contains(val) {
                letter_counts[*val as usize] += 1;
                counts_used.insert(*val);
            }
        }
    }
    println!("Checksum: {:?}", letter_counts[2] * letter_counts[3]);

    let lookup_lines = lines.clone();
    for line in &lines {
        for lookup_line in &lookup_lines {
            let mut differences = 0;
            let line_chars: Vec<char> = line.chars().collect();
            let lookup_line_chars: Vec<char> = lookup_line.chars().collect();
            let mut difference_indices: Vec<usize> = Vec::new();
            let mut line_mut = line.clone();

            for index in 0..line.len() {
                if line_chars[index] != lookup_line_chars[index] {
                    difference_indices.push(index);
                    differences += 1;
                }
            }
            if differences == 1 {
                println!("Close id: {}, {}", line, lookup_line);
                line_mut.remove(difference_indices[0]);
                println!("Same characters: {}", line_mut);
                return;
            }
        }
    }
}
