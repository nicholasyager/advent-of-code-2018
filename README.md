# Advent of Code 2018

Here are my rust answers for the 2018 Advent of Code. They're probably a little rough around the edges, but they all
work.

In each `day_xx` folder, you'll find a README containing the puzzle, my input values, and my solution to the problem.
