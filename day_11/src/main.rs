#[macro_use(s)]
extern crate ndarray;

use ndarray::Array2;
use std::env;

const WIDTH: u32 = 300;
const HEIGHT: u32 = 300;

fn get_hundreds_place(x: i32) -> i32 {
    ((x % 1000) - (x % 100)) / 100
}

fn calculate_power_level(x: i32, y: i32, serial_number: i32) -> i32 {
    let rack_id = x + 10;
    get_hundreds_place(((rack_id * y) + serial_number) * rack_id) - 5
}

fn main() {
    let args: Vec<String> = env::args().collect();
    let serial_number: u32 = args
        .get(1)
        .expect("You must supply a serial number.")
        .parse::<u32>()
        .unwrap();

    // Generate power levels
    println!("Generating power levels.");
    let mut power_meter = Array2::zeros((300, 300));
    for x in 0..WIDTH {
        for y in 0..HEIGHT {
            let power_level = calculate_power_level(x as i32, y as i32, serial_number as i32);
            power_meter[[x as usize, y as usize]] = power_level;
        }
    }

    // Search for the strongest 3x3 square. We will iterate through the grid and
    // sum up the power levels within a square.
    println!("Searching for strongest region.");
    let mut max_power: i32 = -2147483647;
    let mut max_coordinates: (usize, usize) = (0, 0);
    let mut max_region_width: usize = 0;

    for region_width in 3 as usize..300 as usize {
        println!("{}", region_width);
        let mut searches = 0;
        for y in 0..(HEIGHT as usize - region_width) {
            for x in 0..(WIDTH as usize - region_width) {
                let total_power: i32 = power_meter
                    .slice(s![x..(x + region_width), y..(y + region_width)])
                    .sum();

                if total_power > max_power {
                    max_power = total_power;
                    max_coordinates = (x, y);
                    max_region_width = region_width;
                }
                searches += 1;
            }
        }
        println!(
            "\t{}\t{}\t{},{},{}",
            searches, max_power, max_coordinates.0, max_coordinates.1, max_region_width
        );
    }

    println!(
        "The maximum power ({}) was observed at {:?} with a region width of {}.",
        max_power, max_coordinates, max_region_width
    );
}

#[cfg(test)]
mod tests {

    use super::*;
    #[test]
    fn power_level_calculation() {
        assert_eq!(calculate_power_level(3, 5, 8), 4);
    }
    #[test]
    fn get_hundreds_place_calculation() {
        assert_eq!(get_hundreds_place(543210), 2);
    }

    #[test]
    fn get_hundreds_place_calculation_less_than_100() {
        assert_eq!(get_hundreds_place(10), 0);
    }
}
