use std::collections::VecDeque;
use std::env;

fn main() {
    let args: Vec<String> = env::args().collect();
    let player_count: usize = args
        .get(1)
        .expect("You must supply a player count.")
        .parse::<usize>()
        .expect("Not a valid unsigned integer.");

    let last_marble_score: usize = args
        .get(2)
        .expect("You must supply a last marble score.")
        .parse::<usize>()
        .expect("Not a valid unsigned integer.");

    println!(
        "{} players; last marble is worth {} points",
        player_count, last_marble_score
    );

    let mut players: Vec<u64> = vec![0; player_count];
    let mut circle: VecDeque<u32> = VecDeque::with_capacity(last_marble_score);
    circle.push_back(0);

    for marble in 1..last_marble_score {
        let current_player_index = marble % player_count;
        if marble % 23 == 0 {
            players[current_player_index] += marble as u64;

            for _ in 0..7 {
                let back = circle.pop_back().unwrap();
                circle.push_front(back);
            }
            players[current_player_index] += circle.pop_front().unwrap() as u64;
        } else {
            for _ in 0..2 {
                let front = circle.pop_front().unwrap();
                circle.push_back(front);
            }
            circle.push_front(marble as u32)
        }
    }
    println!("The winning score was {}", players.iter().max().unwrap());
}
