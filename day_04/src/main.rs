use std::env;
use std::fs::File;
use std::io::BufRead;
use std::io::BufReader;
extern crate regex;

use regex::Regex;
use std::collections::HashMap;

#[derive(Debug)]
struct Record {
    year: u32,
    month: u32,
    day: u32,
    hour: u32,
    minute: u32,
    message: String,
}

impl Record {
    fn new(input: String) -> Record {
        let re = Regex::new(
            "\\[(?P<year>[0-9]{4})-(?P<month>[0-9]{2})-(?P<day>[0-9]{2}) (?P<hour>[0-9]{2}):(?P<minute>[0-9]{2})\\] (?P<message>.*)$",
        ).unwrap();
        let captures = re.captures(&input).unwrap();
        Record {
            year: captures["year"].parse().unwrap(),
            month: captures["month"].parse().unwrap(),
            day: captures["day"].parse().unwrap(),
            hour: captures["hour"].parse().unwrap(),
            minute: captures["minute"].parse().unwrap(),
            message: captures["message"].to_string(),
        }
    }

    fn timestamp(&self) -> String {
        format!(
            "{:04}-{:02}-{:02} {:02}:{:02}",
            self.year, self.month, self.day, self.hour, self.minute
        )
    }
}

fn main() {
    let args: Vec<String> = env::args().collect();
    let filename: &String = args.get(1).expect("You must supply a file to evaluate.");

    let f = File::open(filename).expect("File not found");
    let file = BufReader::new(&f);
    println!("Processing file {:?}", filename);

    let mut records: Vec<Record> = file.lines().map(|l| Record::new(l.unwrap())).collect();
    records.sort_by(|a, b| a.timestamp().cmp(&b.timestamp()));

    let mut guard_sleep_schedule: HashMap<u32, [u32; 60]> = HashMap::new();

    // Strategy 1: Find the guard that has the most minutes asleep. What minute does that guard spend asleep the most?
    let guard_on_duty = Regex::new("Guard \\#(?P<id>[0-9]+)").unwrap();
    let guard_asleep = Regex::new("asleep").unwrap();
    let guard_awake = Regex::new("wakes").unwrap();
    let mut current_guard = 0;
    let mut time_asleep = 0;
    for record in &records {
        match guard_on_duty.captures(&record.message) {
            Some(captures) => current_guard = captures["id"].parse().unwrap(),
            _ => {}
        }

        if guard_asleep.is_match(&record.message) {
            time_asleep = record.minute;
        } else if guard_awake.is_match(&record.message) {
            let mut guard_schedule: [u32; 60] = match guard_sleep_schedule.get(&current_guard) {
                Some(schedule) => schedule.clone(),
                None => [0_u32; 60],
            };

            for minute in time_asleep..record.minute {
                guard_schedule[minute as usize] += 1;
            }
            guard_sleep_schedule.insert(current_guard, guard_schedule);
        }
    }

    let mut max_sleep = 0_u32;
    let mut sleepiest_guard = 0_u32;
    let mut best_minute = 0_u32;
    for (guard, schedule) in &guard_sleep_schedule {
        let sleep_duration = schedule.iter().fold(0, |acc, x| acc + x);
        let sleepiest_moment = schedule
            .iter()
            .enumerate()
            .map(|(x, y)| (y, x))
            .max()
            .unwrap();

        if sleep_duration > max_sleep {
            max_sleep = sleep_duration;
            sleepiest_guard = *guard;
            best_minute = sleepiest_moment.1 as u32;
        }
    }
    println!(
        "The sleepiest guard is guard #{}\nThe best time is 00:{:01}",
        sleepiest_guard, best_minute
    );
    println!("Checksum: {}", sleepiest_guard * best_minute);

    // Strategy 2: Of all guards, which guard is most frequently asleep on the same minute?
    let mut max_sleep = 0_u32;
    let mut sleepiest_guard = 0_u32;
    let mut best_minute = 0_u32;
    for (guard, schedule) in guard_sleep_schedule {
        let sleepiest_moment = schedule
            .iter()
            .enumerate()
            .map(|(x, y)| (y, x))
            .max()
            .unwrap();

        if sleepiest_moment.0 > &max_sleep {
            max_sleep = *sleepiest_moment.0;
            sleepiest_guard = guard;
            best_minute = sleepiest_moment.1 as u32;
        }
    }
    println!(
        "The guard with the sleepiest moment is guard #{}\nThe best time is 00:{:01}",
        sleepiest_guard, best_minute
    );
    println!("Checksum: {}", sleepiest_guard * best_minute);
}
