extern crate regex;
use std::collections::HashSet;
use std::env;
use std::fs::File;
use std::io::BufRead;
use std::io::BufReader;

use regex::Regex;
use std::ops::{Add, Sub};

#[derive(Debug, Clone)]
struct Vector {
    x: i32,
    y: i32,
}

impl Add for Vector {
    type Output = Vector;

    fn add(self, other: Vector) -> Vector {
        Vector {
            x: self.x + other.x,
            y: self.y + other.y,
        }
    }
}
impl Sub for Vector {
    type Output = Vector;

    fn sub(self, other: Vector) -> Vector {
        Vector {
            x: self.x - other.x,
            y: self.y - other.y,
        }
    }
}

#[derive(Debug)]
struct Light {
    position: Vector,
    velocity: Vector,
}

impl Light {
    fn new(x: i32, y: i32, x_velocity: i32, y_velocity: i32) -> Light {
        Light {
            position: Vector { x, y },
            velocity: Vector {
                x: x_velocity,
                y: y_velocity,
            },
        }
    }

    fn update(&mut self) {
        self.position = self.position.clone() + self.velocity.clone();
    }

    fn backup(&mut self) {
        self.position = self.position.clone() - self.velocity.clone();
    }
}

fn main() {
    let args: Vec<String> = env::args().collect();
    let filename: &String = args.get(1).expect("You must supply a file to evaluate.");

    let f = File::open(filename).expect("File not found");
    let file = BufReader::new(&f);
    println!("Processing file {:?}", filename);

    let light_re = Regex::new(
        "position=< ?(?P<x>[0-9\\-]+), +(?P<y>[0-9\\- ]+)> velocity=< ?(?P<x_velocity>[0-9\\- ]+), +(?P<y_velocity>[0-9\\- ]+)>"
    ).unwrap();

    let lines: Vec<String> = file.lines().map(|l| l.unwrap()).collect();
    let mut lights: Vec<Light> = lines
        .iter()
        .map(|line| {
            let captures = light_re.captures(&line).unwrap();
            Light::new(
                captures["x"].parse::<i32>().unwrap(),
                captures["y"].parse::<i32>().unwrap(),
                captures["x_velocity"].parse::<i32>().unwrap(),
                captures["y_velocity"].parse::<i32>().unwrap(),
            )
        })
        .collect();

    let mut area: i64 = 9999999999999;
    let mut message: String;
    let mut time = 0;
    loop {
        for light in &mut lights {
            light.update();
        }

        let point_coordinates: HashSet<(i32, i32)> = lights
            .iter()
            .map(|l| (l.position.x, l.position.y))
            .collect();

        let mut min_x: i32 = 2147483647;
        let mut min_y: i32 = 2147483647;
        let mut max_x: i32 = -2147483647;
        let mut max_y: i32 = -2147483647;

        for (x, y) in point_coordinates {
            if x < min_x {
                min_x = x;
            }
            if x > max_x {
                max_x = x;
            }
            if y < min_y {
                min_y = y;
            }
            if y > max_y {
                max_y = y;
            }
        }

        let new_area = (max_x as i64 - min_x as i64) * (max_y as i64 - min_y as i64);
        if new_area >= area {
            for light in &mut lights {
                light.backup();
            }
            let point_coordinates: HashSet<(i32, i32)> = lights
                .iter()
                .map(|l| (l.position.x, l.position.y))
                .collect();

            message = String::new();
            for y in min_y..max_y {
                for x in min_x..max_x {
                    message += match point_coordinates.get(&(x, y)) {
                        Some(_) => "#",
                        None => ".",
                    };
                }
                message += "\n";
            }
            println!("{}", message);
            break;
        } else {
            area = new_area;
            time += 1;
        }
    }
    println!("Message seen in {} seconds.", time);
}
