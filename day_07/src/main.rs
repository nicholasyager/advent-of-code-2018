use std::env;
use std::fs::File;
use std::io::BufRead;
use std::io::BufReader;
extern crate regex;

use regex::Regex;
use std::collections::{HashMap, HashSet};

fn available_first_search(
    graph: &HashMap<char, Vec<char>>,
    reverse_graph: &HashMap<char, Vec<char>>,
    vertex: &char,
    visited: &mut HashSet<char>,
    available: &mut HashSet<char>,
) -> Vec<char> {
    visited.insert(*vertex);

    let mut vertices: Vec<char> = Vec::new();
    vertices.push(vertex.clone());
    for neighbor in graph.get(vertex).unwrap() {
        let dependencies_completed = reverse_graph
            .get(neighbor)
            .unwrap()
            .iter()
            .all(|node| visited.contains(node));

        if visited.contains(neighbor) || !dependencies_completed {
            continue;
        }
        available.insert(*neighbor);
    }

    while !available.is_empty() {
        let current_available = available.clone();
        let mut sorted_available = current_available.iter().collect::<Vec<&char>>();
        sorted_available.sort();

        let search_vertex = available.take(sorted_available[0]).unwrap();
        let returned_vertices =
            available_first_search(graph, reverse_graph, &search_vertex, visited, available);
        for returned_vertex in returned_vertices {
            vertices.push(returned_vertex);
        }
    }

    vertices
}

#[derive(Debug)]
struct Worker {
    task: Option<char>,
    time_remaining: u32,
}

const CHAR_ARRAY: [char; 26] = [
    'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S',
    'T', 'U', 'V', 'W', 'X', 'Y', 'Z',
];

const WORKERS: usize = 5;
const TIME_OFFSET: u32 = 60;

fn main() {
    let args: Vec<String> = env::args().collect();
    let filename: &String = args.get(1).expect("You must supply a file to evaluate.");

    let f = File::open(filename).expect("File not found");
    let file = BufReader::new(&f);
    println!("Processing file {:?}", filename);

    let re = Regex::new(
        "Step (?P<source>[A-Z]+) must be finished before step (?P<destination>[A-Z]+) can begin.",
    )
    .unwrap();

    let mut graph: HashMap<char, Vec<char>> = HashMap::new();
    let mut reverse_graph: HashMap<char, Vec<char>> = HashMap::new();
    for line in file.lines().map(|l| l.unwrap()) {
        let captures = re.captures(&line).unwrap();
        let source: char = captures["source"].parse().unwrap();
        let destination: char = captures["destination"].parse().unwrap();
        graph.entry(source).or_insert(Vec::new()).push(destination);
        graph.entry(destination).or_insert(Vec::new());

        reverse_graph
            .entry(destination)
            .or_insert(Vec::new())
            .push(source);
        reverse_graph.entry(source).or_insert(Vec::new());
    }

    // Find the mother vertex. This will be the vertex without any in-bound edges.
    let mut mother_vertices: Vec<&char> = reverse_graph
        .iter()
        .filter(|(_, edges)| edges.is_empty())
        .map(|(vertex, _)| vertex)
        .collect();
    mother_vertices.sort();

    let mut visited = HashSet::new();
    let mut available = HashSet::new();
    let mut global_operations: Vec<char> = Vec::new();
    for mother_vertex in &mother_vertices {
        global_operations.append(&mut available_first_search(
            &graph,
            &reverse_graph,
            mother_vertex,
            &mut visited,
            &mut available,
        ));
    }

    let operation_string = global_operations.iter().collect::<String>();
    assert_eq!(operation_string, "HEGMPOAWBFCDITVXYZRKUQNSLJ");
    println!("Part One: {}", operation_string);

    // Simulate a multithreded process of constructing the sleigh.
    let mut tick: u32 = 0;
    let mut steps_available: HashSet<&char> =
        mother_vertices.iter().map(|c| *c).collect::<HashSet<_>>();
    let mut workers: Vec<Worker> = (0..WORKERS)
        .into_iter()
        .map(|_| Worker {
            task: None,
            time_remaining: 0_u32,
        })
        .collect();

    let mut tasks_completed: HashSet<char> = HashSet::new();
    let mut tasks_in_progress: HashSet<char> = HashSet::new();
    loop {
        for worker in &mut workers {
            if worker.time_remaining >= 1 {
                worker.time_remaining -= 1;
                continue;
            }

            match worker.task {
                Some(task) => {
                    tasks_completed.insert(task);
                    for neighbor in graph.get(&task).unwrap() {
                        let dependencies_completed = reverse_graph
                            .get(neighbor)
                            .unwrap()
                            .iter()
                            .all(|node| tasks_completed.contains(node));
                        if dependencies_completed
                            && !tasks_completed.contains(neighbor)
                            && !tasks_in_progress.contains(neighbor)
                        {
                            steps_available.insert(neighbor);
                        }
                    }
                    worker.task = None;
                }
                None => {}
            }

            let loop_steps = steps_available.clone();
            let mut current_steps = loop_steps.iter().map(|c| *c).collect::<Vec<&char>>();

            if current_steps.is_empty() {
                continue;
            }
            current_steps.sort();

            let new_task = *current_steps.remove(0);
            worker.task = Some(new_task);
            steps_available.remove(&new_task);
            worker.time_remaining =
                TIME_OFFSET + CHAR_ARRAY.iter().position(|c| *c == new_task).unwrap() as u32;
            tasks_in_progress.insert(new_task);
        }

        // Little bit of code to print a schedule diagram
        // print!("{}\t", tick);
        // for worker in &workers {
        //     print!(
        //         "{}\t",
        //         match worker.task {
        //             Some(task) => task,
        //             None => '.',
        //         }
        //     );
        // }
        // print!(
        //     "{}\t{}\n",
        //     tasks_completed.iter().collect::<String>(),
        //     steps_available.iter().map(|c| *c).collect::<String>()
        // );

        if tasks_completed.len() == global_operations.len() {
            break;
        }
        tick += 1;
    }
    assert_eq!(tick - 1, 1226);
    println!("Total work time: {}", tick - 1);
}
