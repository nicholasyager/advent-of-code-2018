use std::collections::HashSet;
use std::env;
use std::fs::File;
use std::io::prelude::*;

fn reduce_polymer(polymer: &mut Vec<char>) {
    let mut start_point: usize = 0;
    loop {
        let mut reactive_sites: Vec<usize> = Vec::new();
        for index in start_point..polymer.len() - 1 {
            let reactant_1 = (polymer[index].to_lowercase(), polymer[index].is_uppercase());
            let reactant_2 = (
                polymer[index + 1].to_lowercase(),
                polymer[index + 1].is_uppercase(),
            );

            if reactant_1.0.to_string() == reactant_2.0.to_string() && reactant_1.1 != reactant_2.1
            {
                reactive_sites.push(index);
                reactive_sites.push(index + 1);

                if index >= 1 {
                    start_point = index - 1;
                } else {
                    start_point = index;
                }
                break;
            }
        }

        if reactive_sites.len() == 0 {
            return;
        }

        reactive_sites.sort();
        reactive_sites.reverse();
        for site in reactive_sites {
            polymer.remove(site);
        }
    }
}

fn main() {
    let args: Vec<String> = env::args().collect();
    let filename: &String = args.get(1).expect("You must supply a file to evaluate.");

    let mut file = File::open(filename).expect("File not found");
    let mut input = String::new();
    file.read_to_string(&mut input)
        .expect("Unable to read the file");

    let original_polymer: Vec<char> = input.chars().collect();
    let mut polymer = original_polymer.clone();

    println!("Part One:");
    reduce_polymer(&mut polymer);
    println!("{}", polymer.iter().collect::<String>());
    println!("{}", polymer.len());

    println!("Part Two:");
    let mut smallest_polymer: String = original_polymer.iter().collect();
    let mut reaction_types: HashSet<String> = HashSet::new();

    for character in &original_polymer {
        reaction_types.insert(character.to_lowercase().to_string());
    }

    for reaction in reaction_types {
        let mut polymer = original_polymer.clone();

        let mut reactive_sites: Vec<usize> = Vec::new();
        for index in 0..polymer.len() {
            if polymer[index].to_lowercase().to_string() == reaction {
                reactive_sites.push(index)
            }
        }
        reactive_sites.sort();
        reactive_sites.reverse();
        for site in reactive_sites {
            polymer.remove(site);
        }

        // React as per usual
        reduce_polymer(&mut polymer);
        if polymer.len() < smallest_polymer.len() {
            smallest_polymer = polymer.iter().collect();
        }
    }
    println!("{}", smallest_polymer);
    println!("{}", smallest_polymer.len());
}
