use std::env;
use std::fs::File;
use std::io::BufRead;
use std::io::BufReader;

#[derive(Debug)]
struct Node {
    children: Vec<Node>,
    metadata: Vec<u32>,
}

impl Node {
    fn parse(input: &mut impl Iterator<Item = u32>) -> Option<Node> {
        // Parse out the number of children
        let child_count = match input.next() {
            Some(child_count) => child_count,
            None => return None,
        };

        // Parse out the amount of metadata
        let metadata_count = match input.next() {
            Some(metadata_count) => metadata_count,
            None => return None,
        };

        // Create the node
        let mut node = Node {
            children: Vec::new(),
            metadata: Vec::new(),
        };

        // Continue parsing the iterator for each of our `child_count` children
        for _ in 0..child_count {
            node.children.extend(Self::parse(input));
        }

        // Finish by parsing out the metadata at the end of the stream.
        for _ in 0..metadata_count {
            node.metadata.push(input.next().unwrap());
        }

        Some(node)
    }

    fn total_data(&self) -> u32 {
        self.metadata.iter().sum::<u32>() + self
            .children
            .iter()
            .map(|child| child.total_data())
            .sum::<u32>()
    }

    fn weighted_data(&self) -> u32 {
        if self.children.is_empty() {
            self.metadata.iter().cloned().sum::<u32>()
        } else {
            let mut weighted_total = 0;

            for m in self.metadata.iter().cloned() {
                weighted_total += self
                    .children
                    .get(m as usize - 1)
                    .map(|c| c.weighted_data())
                    .unwrap_or_default();
            }

            weighted_total
        }
    }
}

fn main() {
    let args: Vec<String> = env::args().collect();
    let filename: &String = args.get(1).expect("You must supply a file to evaluate.");

    let f = File::open(filename).expect("File not found");
    let file = BufReader::new(&f);
    println!("Processing file {:?}", filename);

    let lines: Vec<String> = file.lines().map(|l| l.unwrap()).collect();
    let mut input = lines
        .iter()
        .flat_map(|s| s.split(' '))
        .map(|s| s.parse::<u32>().unwrap());

    let tree = Node::parse(&mut input).unwrap();
    println!("{:?}", tree);
    println!("Total data: {}", tree.total_data());
    println!("Weighted data: {}", tree.weighted_data());
}
