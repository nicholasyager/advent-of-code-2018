use std::env;
use std::fs::File;
use std::io::BufRead;
use std::io::BufReader;
extern crate regex;

use regex::Regex;
use std::collections::{HashMap, HashSet};

fn main() {
    let args: Vec<String> = env::args().collect();
    let filename: &String = args.get(1).expect("You must supply a file to evaluate.");

    let f = File::open(filename).expect("File not found");
    let file = BufReader::new(&f);
    println!("Processing file {:?}", filename);

    let mut fabric: HashMap<(u32, u32), HashSet<u32>> = HashMap::new();
    let mut uncontexted_claims: HashSet<u32> = HashSet::new();

    let re = Regex::new(
        "\\#(?P<id>[0-9]+) @ (?P<x>[0-9]+),(?P<y>[0-9]+): (?P<width>[0-9]+)x(?P<height>[0-9]+)",
    )
    .unwrap();

    for line in file.lines().map(|l| l.unwrap()) {
        let captures = re.captures(&line).unwrap();
        let id: u32 = captures["id"].parse().unwrap();
        uncontexted_claims.insert(id);
        let x: u32 = captures["x"].parse().unwrap();
        let y: u32 = captures["y"].parse().unwrap();
        for inch_y in y..y + captures["height"].parse::<u32>().unwrap() {
            for inch_x in x..x + captures["width"].parse::<u32>().unwrap() {
                let claims = fabric.entry((inch_x, inch_y)).or_insert(HashSet::new());
                claims.insert(id);
                if claims.len() > 1 {
                    for claim in claims.iter() {
                        uncontexted_claims.remove(claim);
                    }
                }
            }
        }
    }

    // I've been thinking about different ways of accomplishing this problem.
    // One possible alternative would be to construct a vector of hashsets which
    // I can then compute the intersections of. Part one would be solved by storing
    // The length of the set of coordinates which are the intersection of any two
    // of the sets. Part two would the the only set that has no intersections with
    // any other sets. While interesting, I think that algorithmic complexity would
    // be higher for that approach since it would grow exponentially for each
    // additional claim.

    let mut multiple_claims: u32 = 0;
    // Inches within 2 or more claims
    for (_, claims) in fabric {
        if claims.len() >= 2 {
            multiple_claims += 1;
        }
    }
    println!(
        "There are {} inches in two or more claims.",
        multiple_claims
    );
    println!("{:?}", uncontexted_claims);
}
