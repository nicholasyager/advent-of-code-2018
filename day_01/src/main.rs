use std::env;
use std::fs::File;
use std::io::BufRead;
use std::io::BufReader;

use std::collections::HashSet;

fn main() {
    let args: Vec<String> = env::args().collect();
    let filename: &String = args.get(1).expect("You must supply a file to evaluate.");

    let f = File::open(filename).expect("File not found");
    let file = BufReader::new(&f);
    let lines: Vec<String> = file.lines().map(|l| l.unwrap()).collect();
    println!("Processing file {:?}", filename);

    let mut frequency: i32 = 0;
    let mut known_frequencies: HashSet<i32> = HashSet::new();
    let mut first = true;
    loop {
        for line in &lines {
            let parts = line.split_whitespace().collect::<Vec<&str>>();

            for part in parts {
                let mut part_string: String = String::from(part);
                let sign = part_string.remove(0);
                let mut change: i32 = part_string.parse().unwrap();
                if sign == '-' {
                    change *= -1;
                }
                frequency += change;

                if known_frequencies.contains(&frequency) {
                    println!("Part 2: {:?}", frequency);
                    return;
                }

                known_frequencies.insert(frequency);
            }
        }
        if first {
            println!("Part 1: {:?}", frequency);
            first = false;
        }
    }
}
